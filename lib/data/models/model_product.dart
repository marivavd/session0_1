class ModelProduct{
  final String id;
  final String title;
  final String categoryId;
  final String description;
  final double cost;
  final List<dynamic> covers;
  final bool best;
  bool isFavourite;
  bool isBasket;
  int count;

  ModelProduct({required this.id, required this.title, required this.categoryId, required this.best, required this.description, required this.cost, required this.covers, this.count = 1, this.isFavourite = false, this.isBasket = false});

}