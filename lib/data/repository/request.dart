import 'package:session0_1/data/models/model_category.dart';
import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/domain/make_list_models_product.dart';
import 'package:session0_1/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> signIn(String email, String password)async{
  final AuthResponse res = await supabase.auth.signInWithPassword(
    email: email,
    password: password,
  );

}
Future<ModelCategory> getCategorywithid(String id)async{
  var response = await supabase.from('categories').select().eq('id', id);
  List<ModelCategory> result = [];
  for (var val in response){
    result.add(ModelCategory(id: val['id'], title: val['title']));
  }
  return result.first;
}
Future<List<ModelCategory>> getCategories()async{
  var response = await supabase.from('categories').select();
  List<ModelCategory> result = [];
  for (var val in response){
    result.add(ModelCategory(id: val['id'], title: val['title']));
  }
  return result;
}
Future<List<ModelProduct>> getBestProducts()async{
  var response = await supabase.from('products').select().eq('is_best_seller', true);
  List<ModelProduct> result = await makeProductsModels(response);
  return result;
}

Future<List<ModelProduct>> getWholeProducts()async{
  var response = await supabase.from('products').select();
  List<ModelProduct> result = await makeProductsModels(response);
  return result;
}

Future<List<String>> getImageofProduct(String id)async{
  //return await supabase.storage.from('ProductImages').getPublicUrl('${id}-${color}.png');
  final List<FileObject> objects = await supabase
      .storage
      .from('ProductImages')
      .list();
  List<String> result = [];
  for (var val in objects){
    if (val.name.contains(id)){
      print(val.name);
      result.add(await supabase.storage.from('ProductImages').getPublicUrl(val.name));
    }
  }
  return result;
}

Future<bool> InBasket(String id)async{
  var response = await supabase.from('cart').select().eq('product_id', id).eq('user_id', supabase.auth!.currentUser!.id);
  if (response.isNotEmpty){
    return true;
  }
  else{
    return false;
  }
}

Future<void> AddToBasket(String id)async{
  var response = await supabase.from('cart').insert({'product_id': id, 'user_id': supabase.auth!.currentUser!.id, 'count': 1});

}
Future<void> removeBasket(String id)async{
  var response = await supabase.from('cart').delete().match({'product_id': id, 'user_id': supabase.auth!.currentUser!.id, });

}
Future<void> UpdateproductInBasket(String id, int count)async{
  var response = await supabase.from('cart').update({'count': count}).eq('product_id', id).eq('user_id', supabase.auth!.currentUser!.id);
}
Future<int> getCount(String id)async{
  var response = await supabase.from('cart').select().eq('product_id', id).eq('user_id', supabase.auth!.currentUser!.id);
  if (response.length == 0){
    return 0;
  }
  else{
    return response.first['count'];
  }
}