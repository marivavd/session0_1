import 'package:session0_1/data/storage/shared_preferences.dart';
import 'package:session0_1/domain/to_json.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> initShared()async{
  sharedPreferences = await SharedPreferences.getInstance();
}
List<String> getAllProducts(){
  return sharedPreferences?.getStringList('AllProjects') ?? [];
}
Future<void> setAllProducts()async{
  sharedPreferences?.setStringList('AllProjects', toJsonProducts());
}

