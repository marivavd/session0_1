import 'dart:async';
import 'dart:async';

import 'package:session0_1/data/repository/request.dart';
import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/domain/utils.dart';
import 'package:session0_1/main.dart';

class CartUseCase {
  Future<void> makeInCart(String id, Function(void) onResponse,
      Future<void> Function(String) onError) async {
    requestSignIn()async{
      await AddToBasket(id);
    }
    requests(requestSignIn, onResponse, onError);
  }
  Future<void> updateCountInCart(String id, int count, Function(void) onResponse,
  Future<void> Function(String) onError) async {
  requestSignIn()async{
    await UpdateproductInBasket(id, count);
    }
  requests(requestSignIn, onResponse, onError);
}
  Future<void> isCart(String id, Function(dynamic) onResponse,
      Future<void> Function(String) onError) async {
    requestSignIn()async{
      return await InBasket(id);
    }
    requests(requestSignIn, onResponse, onError);
  }

  Future<void> removeCart(String id, Function(dynamic) onResponse,
      Future<void> Function(String) onError) async {
    requestSignIn()async{
      return await removeBasket(id);
    }
    requests(requestSignIn, onResponse, onError);
  }


}