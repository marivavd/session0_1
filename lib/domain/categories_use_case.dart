import 'package:session0_1/data/repository/request.dart';
import 'package:session0_1/domain/utils.dart';

class CategoriesUseCase{
  Future<void> getAllCategories(Function(dynamic) onResponse, Future<void> Function(String) onError)async{

    requestSignIn()async{
      return await getCategories();
    }
    requests(requestSignIn, onResponse, onError);
  }

  Future<void> getCategoryById(String id, Function(dynamic) onResponse, Future<void> Function(String) onError)async{

    requestSignIn()async{
      return await getCategorywithid(id);
    }
    requests(requestSignIn, onResponse, onError);
  }
}