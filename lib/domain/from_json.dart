import 'dart:convert';

import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/data/storage/all_projects.dart';

void fromJsonProducts(){
  List<String> response = getAllProducts();

  for (var val in response){
    Map<String, dynamic> map = jsonDecode(val);
    allProducts.add(ModelProduct
      (id: map['id'],
        title: map['title'],
        categoryId: map['categoryId'],
        cost: map['cost'],
        description: map['description'],
        covers: map['covers'],
        isFavourite: map['isFavourite'],
        isBasket: map['isBasket'],
        count: map['count'],
        best: map['best']));
  }
}