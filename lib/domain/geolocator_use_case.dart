import 'package:geolocator/geolocator.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class GeolocatorUseCase{
  Future<void> getPosition(Function(Position) onResponse, Future<void> Function(String) onError)async{
    LocationPermission permission = await Geolocator.checkPermission();
    if (!(await Geolocator.isLocationServiceEnabled())){
      onError('Location no');
      return;
    }
    if (permission == LocationPermission.deniedForever || permission == LocationPermission.denied){
      onError("Denied");
      return;
    }
    var position = await Geolocator.getCurrentPosition();
    onResponse(position);
  }

  Future<void> getAddress(Point point, Function(String) onResponse, Future<void> Function(String) onError)async{
    var response = YandexSearch.searchByPoint(
        point: point, searchOptions: SearchOptions(
      geometry: false,
      searchType: SearchType.geo
    ));
    var result = await response.result;
    var firstResult = result.items?.firstOrNull?.name;
    if (firstResult == null){
      onError('No address');
      return;
    }
    onResponse(firstResult);
  }
}
