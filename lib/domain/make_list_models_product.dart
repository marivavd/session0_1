import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/data/repository/request.dart';

Future<List<ModelProduct>> makeProductsModels(data)async{
  List<ModelProduct> result = [];
  for (var val in data){
    List<String> covers = await getImageofProduct(val['id']);
    result.add(ModelProduct(id: val['id'], title: val['title'], best:val['is_best_seller'], categoryId: val['category_id'], cost: double.parse(val['cost'].toString()), description: val['description'], covers: covers, isBasket: await InBasket(val['id']), count: await getCount(val['id'])));
  }
  return result;
}