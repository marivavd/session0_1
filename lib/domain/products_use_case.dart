import 'package:session0_1/data/repository/request.dart';
import 'package:session0_1/domain/utils.dart';

class ProductsUseCase{
  Future<void> getModelsBestProducts(Function(dynamic) onResponse, Future<void> Function(String) onError)async{

    requestSignIn()async{
      return await getBestProducts();
    }
    requests(requestSignIn, onResponse, onError);
  }

  Future<void> getModelsProducts(Function(dynamic) onResponse, Future<void> Function(String) onError)async{

    requestSignIn()async{
      return await getWholeProducts();
    }
    requests(requestSignIn, onResponse, onError);
  }

}