import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/domain/utils.dart';

class SharedUseCase{
  List<String> getSharedProducts(){
      return getAllProducts();
  }

  Future<void> setSharedProducts()async{
      return await setAllProducts();
  }
}