import 'package:session0_1/data/repository/request.dart';
import 'package:session0_1/domain/check.dart';
import 'package:session0_1/domain/utils.dart';

class SignInUseCase{
  Check check = Check();
  Future<void> pressButton(String email, String password, Function(void) onResponse, Future<void> Function(String) onError)async{
    if (check.checkEmail(email) == 0){
      onError('Email incorrect');
      return;
    }
    if (check.checkPassword(password) ==0){
      onError('Password is empty');
      return;
    }
    requestSignIn()async{
      await signIn(email, password);
    }
    requests(requestSignIn, onResponse, onError);
  }
}