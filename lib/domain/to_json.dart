import 'dart:convert';

import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/data/storage/all_projects.dart';

List<String> toJsonProducts(){
  List<String> response = [];

  for (var val in allProducts){
    response.add(
      jsonEncode(
        {
          'id': val.id,
          'title': val.title,
          'categoryId': val.categoryId,
          'cost': val.cost,
          'description': val.description,
          'covers': val.covers,
          'isFavourite': val.isFavourite,
          'isBasket': val.isBasket,
          'count': val.count,
          'best': val.best
        }
      )
    );
  }
  return response;
}