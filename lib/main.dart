import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/pages/home.dart';
import 'package:session0_1/presentation/pages/sign_in.dart';
import 'package:session0_1/presentation/widgets/card_product.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initShared();
  await Supabase.initialize(
    url: 'https://muozgansistgyudgofqc.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im11b3pnYW5zaXN0Z3l1ZGdvZnFjIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTQ5ODgzNDcsImV4cCI6MjAzMDU2NDM0N30.7RdYIRhMPaOtQj4kcd1JYuhMQ-hrSgnlkXkA5TEZgk4',
  );

  runApp(MyApp());
}

final supabase = Supabase.instance.client;

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver{
  FlutterLocalization localization = FlutterLocalization.instance;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    localization.init(mapLocales: [MapLocale('ru', LocaleApp.RU),
    MapLocale('en', LocaleApp.EN)],
        initLanguageCode: (Platform.localeName.split("_").first != 'ru')?'en':'ru');
    localization.onTranslatedLanguage = (_) => setState(() {

    });
  }

  @override
  void didChangeLocales(List<Locale>? locales) {
    // TODO: implement didChangeLocales

    if (locales == null){
      return;
    }
      localization.translate((locales.first.languageCode != 'ru')?'en':'ru');
    super.didChangeLocales(locales);

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: Size(375, 812),
        builder: (_, i){
          return MaterialApp(
            title: 'Flutter Demo',
            supportedLocales: localization.supportedLocales,
            localizationsDelegates: localization.localizationsDelegates,
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            home: SignIn(),
          );
        }
    );
  }
}

