mixin LocaleApp{
  static const String signinTitle = 'signinTitle';
  static const String signinText = 'signinText';
  static const String email = 'email';
  static const String password = 'password';
  static const String signinButton = 'signinButton';
  static const String recovery = 'recovery';
  static const String areYouFirst = 'areYouFirst';
  static const String createAccount = 'createAccount';
  static const String main = 'main';
  static const String search = 'search';
  static const String categories = 'categories';
  static const String popular = 'popular';
  static const String all = 'all';
  static const String actions = 'all';
  static const String cart = 'cart';



  static const Map<String, dynamic> RU ={
    signinTitle: 'Привет!',
    signinText: 'Заполните Свои Данные Или\nПродолжите Через Социальные Медиа',
    signinButton: 'Войти',
    recovery: 'Востановить',
    email: 'Email',
    password: 'Пароль',
    areYouFirst: 'Вы впервые? ',
    createAccount: 'Создать пользователя',
    main: 'Главная',
    search: 'Поиск',
    categories: 'Категории',
    popular: 'Популярное',
    all: 'Все',
    cart: 'Корзина'
  };

  static const Map<String, dynamic> EN ={
    signinTitle: 'Hello Again!',
    signinText: 'Fill Your Details Or Continue With\nSocial Media',
    signinButton: 'Sign In',
    recovery: 'Recovery Password',
    email: 'Email Address',
    password: 'Password',
    areYouFirst: 'New User? ',
    createAccount: 'Create Account',
    main: 'Explore',
    search: 'Looking for shoes',
    categories: 'Select Category',
    popular: 'Popular Shoes',
    all: 'See all',
    cart: 'Cart'
  };
}