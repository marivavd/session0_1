import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/data/storage/all_projects.dart';
import 'package:session0_1/domain/cart_use_case.dart';
import 'package:session0_1/domain/products_use_case.dart';
import 'package:session0_1/presentation/dialogs.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/widgets/back_button.dart';

class Cart extends StatefulWidget {
  const Cart({super.key});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  List<ModelProduct> cartProducts = [];
  CartUseCase useCase = CartUseCase();
  ProductsUseCase productUseCase = ProductsUseCase();
  bool flag = false;
  int summa = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await productUseCase.getModelsProducts(
              (response){
                var p0 = response;
                for (var val in p0){
                 if (val.isBasket){
                   cartProducts.add(val);
                   summa += int.parse( val.count.toString());
                 }
                }
                flag = true;
                setState(() {

                });
              },
              (error)async{
                showError(context, error);
              }
      );
    });

  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 48.w,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ButtontoBack(),
                Padding(padding: EdgeInsets.symmetric(vertical: 10.w),child: Text(
                  LocaleApp.cart.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          color: Color(0xFF2B2B2B),
                          height: 24/16.w,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w500
                      )
                  ),
                ),),
                Container(
                  height: 44.w,
                  width: 44.w,
                ),

              ],
            ),
            SizedBox(height: 16.w,),
            Text(
              '${summa} товара'
            ),
            SizedBox(height: 13.w,),
            Expanded(child: ListView.separated(
                itemBuilder: (_, index){
                  return Row(
                    children: [
                      Container(
                        width: 58.w,
                        height: 104.w,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.w),
                          color: Color(0xFF48B2E7)
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 22.w),
                          child: Column(
                            children: [
                              SizedBox(height: 14.w,),
                              InkWell(
                                child: SvgPicture.asset('assets/plus.svg'),
                                onTap: ()async{
                                  showLoading(context);
                                  summa += 1;
                                  if (cartProducts[index].isBasket){
                                    cartProducts[index].count += 1;
                                    useCase.makeInCart(cartProducts[index].id,
                                            (p0){
                                      hideLoading(context);
                                      setState(() {

                                      });
                                      },
                                            (error)async{
                                      hideLoading(context);
                                      showError(context, error);
                                    });
                                  }
                                  else{
                                    cartProducts[index].isBasket = true;
                                    useCase.makeInCart(cartProducts[index].id,
                                            (p0){
                                          hideLoading(context);
                                        },
                                            (error)async{
                                          hideLoading(context);
                                          showError(context, error);
                                        });
                                  }
                                  setState(() {

                                  });

                                },
                              ),
                              SizedBox(height: 23.w,),
                              Text(
                                cartProducts[index].count.toString()
                              ),
                              SizedBox(height: 20.w,),
                             InkWell(
                               onTap: ()async{
                                 if (cartProducts[index].count != 1){
                                 cartProducts[index].count -= 1;
                                 summa -=1;
                                 showLoading(context);
                                 useCase.updateCountInCart(cartProducts[index].id,
                                     cartProducts[index].count,
                                         (p0) {
                                   hideLoading(context);
                                   setState(() {

                                   });
                                         }, (error)async{
                                   hideLoading(context);
                                   showError(context, error);
                                     }
                                 );

                               }},
                                 child:  Container(
                                   height: 14.w,
                                   width: 14.w,
                                   child: SvgPicture.asset('assets/minus.svg'),
                                 )
                             ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width: 10.w,),
                      Container(
                        height: 104.w,
                        width: 199.w,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8)
                        ),
                        child: Text(
                          cartProducts[index].title
                        ),

                      ),
                      SizedBox(width: 10.w,),
                      Container(
                        width: 58.w,
                        height: 104.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.w),
                            color: Color(0xFFF87265)
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: InkWell(
                            child: SvgPicture.asset('assets/musor.svg'),
                            onTap: ()async{;
                              showLoading(context);
                              summa -= cartProducts[index].count;
                              useCase.removeCart(cartProducts[index].id,
                                      (p0) {
                                hideLoading(context);
                                cartProducts.remove(cartProducts[index]);
                                setState(() {

                                });
                                      }, (error)async{
                                hideLoading(context);
                                showError(context, error);
                                  }
                              );
                            },
                          )
                        )
                      ),
                    ],
                  );
                },
                separatorBuilder: (_, index){
                  return SizedBox(height: 14.w,);
                },
                itemCount: cartProducts.length)),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 258.w,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white
                ),
                child: FilledButton(
                  onPressed: (){},
                  child: Text(
                    'Оформить заказ'
                  ),
                  style: FilledButton.styleFrom(
                      backgroundColor: Color(0xFF48B2E7),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.w)
                      )
                  ),
                ),
              ),
            )

          ],
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
