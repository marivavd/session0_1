import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/data/models/model_category.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/widgets/back_button.dart';

class Categories extends StatefulWidget {
  Categories({super.key, required this.category});
  ModelCategory category;

  @override
  State<Categories> createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  ModelCategory? currentCategory;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentCategory = widget.category;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFF7F7F9),
        body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      SizedBox(height: 48.w,),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ButtontoBack(),
          Padding(padding: EdgeInsets.symmetric(vertical: 10.w),child: Text(
            LocaleApp.popular.getString(context),
            style: GoogleFonts.raleway(
                textStyle: TextStyle(
                    color: Color(0xFF2B2B2B),
                    height: 24/16.w,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.w500
                )
            ),
          ),),
          Container(
            height: 44.w,
            width: 44.w,
          ),

        ],
      ),
    ]))
    );
  }
}
