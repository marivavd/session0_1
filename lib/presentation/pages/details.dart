import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/data/models/model_category.dart';
import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/domain/cart_use_case.dart';
import 'package:session0_1/domain/categories_use_case.dart';
import 'package:session0_1/presentation/dialogs.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/widgets/back_button.dart';

class Details extends StatefulWidget {

  ModelProduct product;
  Details({super.key, required this.product});

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  CategoriesUseCase useCase = CategoriesUseCase();
  bool flag = false;
  ModelCategory? category;
  bool isMaxLines = true;
  CartUseCase cartUseCase = CartUseCase();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.getCategoryById(widget.product.categoryId,
              (p0){
        category = p0;
        flag = true;
        setState(() {

        });
              },
              (error)async{
        await showError(context, error);
              }
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 48.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtontoBack(),
                  Padding(padding: EdgeInsets.symmetric(vertical: 10.w),child: Text(
                    'Sneaker Shop',
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF2B2B2B),
                            height: 24/16.w,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.w500
                        )
                    ),
                  ),),
                  Column(
                    children: [
                      Stack(
                        children: [
                          Container(
                            height: 44.w,
                            width: 44.w,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(40.w)
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(10.w),
                              child: SvgPicture.asset('assets/bag-2.svg', height: 24.w, width: 24.w, color: Color(0xFF2B2B2B)),

                            ),
                          ),
                          Padding(padding: EdgeInsets.only(left: 34.w),child: SvgPicture.asset('assets/Ellipse 886.svg', height: 8.w, width: 8.w,),),
                        ],
                      )
                    ],
                  )

                ],
              ),
              SizedBox(height: 38.w,),
              Text(
                widget.product.title,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        color: Color(0xFF2B2B2B),
                        height: 62/52.w,
                        fontSize: 26.sp,
                        fontWeight: FontWeight.w700
                    )
                ),
              ),
              SizedBox(height: 8.w,),
              Text(
                category!.title,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        color: Color(0xFF6A6A6A),
                        height: 19/16.w,
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w500
                    )
                ),
              ),
              SizedBox(height: 8.w,),
              Text(
                '₽${widget.product.cost.toDouble()}',
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        color: Color(0xFF2B2B2B),
                        height: 28/24.w,
                        fontSize: 24.sp,
                        fontWeight: FontWeight.w600
                    )
                ),
              ),
              SizedBox(height: 22.w,),
              CarouselSlider.builder(
                  itemCount: widget.product.covers.length,
                  itemBuilder: (_, index, i){
                    return Container(
                      height: 125.w,
                      width: 241.w,
                      child:  CachedNetworkImage(imageUrl: widget.product.covers[index], fit: BoxFit.cover,),

                    );
                  },
                  options: CarouselOptions(

                  )),

              SizedBox(height: 37.w,),
              SizedBox(
                height: 56.w,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                    itemBuilder: (_, index){
                      return Container(
                        height: 56.w,
                        width: 56.w,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16.w)
                        ),
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 14.5.w),
                          child: CachedNetworkImage(
                            imageUrl: widget.product.covers[index],
                            fit: BoxFit.cover,
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (_, index){
                      return SizedBox(width: 14.w,);
                },
                    itemCount: widget.product.covers.length),
              ),
              SizedBox(height: 33.w,),
              Text(
                widget.product.description,
                maxLines: (isMaxLines)?3:1000,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        color: Color(0xFF6A6A6A),
                        height: 72/42.w,
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w400
                    )
                ),
              ),
              SizedBox(height: 9.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    child: Text(
                      'Подробнее',
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Color(0xFF48B2E7),
                              height: 21/14.w,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.w400
                          )
                      ),
                    ),
                    onTap: (){
                      isMaxLines = !isMaxLines;
                      setState(() {

                      });
                    },
                  )
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          height: 44.w,
                          width: 44.w,
                          decoration: BoxDecoration(
                              color: Color(
                                  0x66D9D9D9),
                              borderRadius: BorderRadius.circular(40)
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 14.w, vertical: 14.w),
                            child: SvgPicture.asset('assets/heart_no.svg', ),
                          ),
                        ),
                        SizedBox(width: 18,),
                        SizedBox(
                          width: 265.w,
                          height: 52.w,
                          child: FilledButton(onPressed: ()async{
                            showLoading(context);
                          if (widget.product.isBasket){
                            widget.product.count += 1;
                            cartUseCase.makeInCart(widget.product.id,
                                    (p0){
                                  hideLoading(context);
                                },
                                    (error)async{
                                  hideLoading(context);
                                  showError(context, error);
                                });
                          }
                          else{
                            widget.product.isBasket = true;
                            cartUseCase.makeInCart(widget.product.id,
                                    (p0){
                                  hideLoading(context);
                                },
                                    (error)async{
                                  hideLoading(context);
                                  showError(context, error);
                                });
                          }
                            setState(() {

                            });
                          }, child: Text((widget.product.isBasket)?'Добавлено':'В корзину',
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    color: Colors.white,
                                    height: 22/14.w,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w600
                                )
                            ),
                          ),
                            style: FilledButton.styleFrom(
                                backgroundColor: Color(0xFF48B2E7),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.w)
                                )
                            ),),
                        )

                      ],
                    ),
                    SizedBox(height: 40.w,)
                  ],
                ),
              )

            ],
          ),
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
