import 'package:flutter/material.dart';
import 'package:session0_1/presentation/pages/cart.dart';
import 'package:session0_1/presentation/pages/holder.dart';
import 'package:session0_1/presentation/pages/main_page.dart';
import 'package:session0_1/presentation/widgets/bottom_navig_bar.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int currentIndex = 0;
  List<Widget> sp = [MainPage(), Holder(), Scaffold( backgroundColor: Color(0xFFF7F7F9)),Scaffold( backgroundColor: Color(0xFFF7F7F9))];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Stack(
        children: [
          sp[currentIndex],
          Align(
            alignment: Alignment.bottomCenter,
            child: BottomNavig(onSelect: (int index){
              setState(() {
                currentIndex = index;
              });
            }, onTapCart: (){
              Navigator.push(context, MaterialPageRoute(builder: (_) => Cart()));
            }),
          )
        ],
        
      ),
    );
  }
}
