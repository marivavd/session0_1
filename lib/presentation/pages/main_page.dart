
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/data/models/model_category.dart';
import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/data/storage/all_projects.dart';
import 'package:session0_1/domain/categories_use_case.dart';
import 'package:session0_1/domain/from_json.dart';
import 'package:session0_1/domain/products_use_case.dart';
import 'package:session0_1/presentation/dialogs.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/pages/popular.dart';
import 'package:session0_1/presentation/widgets/card_product.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<ModelCategory> categories = [];
  List<ModelProduct> products = [];
  bool flag = false;
  CategoriesUseCase useCase = CategoriesUseCase();
  ProductsUseCase productsUseCase = ProductsUseCase();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async {

      await useCase.getAllCategories(
              (p0)async{
                categories = p0;
                categories.insert(0, ModelCategory(id: '0', title: 'Все'));

              },
              (error)async{
                await showError(context, error);
              });
      productsUseCase.getModelsBestProducts(
              (p0){
                products = p0;
                flag = true;
                setState(() {

                });
              },
              (error)async{
                showError(context, error);
              }
      );


    });

  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 48.w,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(padding: EdgeInsets.only(top: 14.w),
                      // child: Column(
                      //   children: [
                      //     SvgPicture.asset('assets/Hamburger.svg'),
                      //     Transform.translate(offset: Offset(0, 4.w),
                      //     child: ImageFiltered(
                      //     imageFilter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                      //       child: SvgPicture.asset('assets/Hamburger.svg'),
                      //
                      // )),]
                        child: SvgPicture.asset('assets/Hamburger.svg', height: 18.w, width: 25.71.w,),
                        ),
                      Padding(padding: EdgeInsets.only(top: 3.w),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Transform.translate(
                                offset: const Offset(3, 0),
                                child: SvgPicture.asset("assets/Highlight_05.svg")
                            ),
                            Text(LocaleApp.main.getString(context),
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xFF2B2B2B),
                                        height: 38/32,
                                        fontSize: 32)
                                ))
                          ],
                        ),),
                      Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                height: 44.w,
                                width: 44.w,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(40.w)
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(10.w),
                                  child: SvgPicture.asset('assets/bag-2.svg', height: 24.w, width: 24.w, color: Color(0xFF2B2B2B)),

                                ),
                              ),
                              Padding(padding: EdgeInsets.only(left: 34.w),child: SvgPicture.asset('assets/Ellipse 886.svg', height: 8.w, width: 8.w,),),
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 19.w,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 52.w,
                        width: 269.w,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(14.w),
                          border: Border.all(color: Colors.transparent)
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 26.w,),
                            Padding(padding: EdgeInsets.symmetric(vertical: 14.w),
                            child:SvgPicture.asset('assets/Vector.svg', height: 14.33.w, width: 14.33.w,),),
                            SizedBox(width: 12.w,),
                            Padding(padding: EdgeInsets.symmetric(vertical: 16.w),
                            child: Text(
                              LocaleApp.search.getString(context),
                              style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  color: Color(0xFF6A6A6A),
                                  fontSize: 12.sp,
                                  height: 20/12.w,

                                )
                              ),
                            ),)

                          ],
                        )
                      ),
                      Container(
                        height: 52.w,
                        width: 52.w,
                        decoration: BoxDecoration(
                          color: Color(0xFF48B2E7),
                          borderRadius: BorderRadius.circular(52.w)
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(14.w),
                          child: SvgPicture.asset('assets/sliders.svg', height: 24.w, width: 24.w,)
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 24.w,),
                  Text(
                    LocaleApp.categories.getString(context),
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          color: Color(0xFF2B2B2B),
                          fontSize: 16.sp,
                          height: 19/16.w,
                          fontWeight: FontWeight.w600

                        )
                    ),
                  ),
                  SizedBox(height: 16.w,),

                ],
              ),
            ),
            Padding(padding: EdgeInsets.only(left: 20.w),
            child: SizedBox(
              height: 40.w,
              child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (_, index){
                    return InkWell(
                      child: Container(
                        height: 40.w,
                        padding: EdgeInsets.symmetric(vertical: 11.w, horizontal: 26.w),
                        width: 108.w,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8.w),
                        ),
                        child: Text(
                          categories[index].title,
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                        color: Color(0xFF2B2B2B),
                          fontSize: 12.sp,
                          height: 18/12.w,

                      )
                    ),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (_, index){
                    return SizedBox(width: 16.w,);
                  },
                  itemCount: categories.length),
            ),),
            SizedBox(height: 35.w,),
            Padding(padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      LocaleApp.popular.getString(context),
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              color: Color(0xFF2B2B2B),
                              fontSize: 16.sp,
                              height: 24/16.w,
                              fontWeight: FontWeight.w500

                          )
                      ),
                    ),
                    InkWell(
                      child: Text(
                        LocaleApp.all.getString(context),
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              color: Color(0xFF48B2E7),
                              fontSize: 12.sp,
                              height: 16/12.w,

                            )
                        ),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (_) => Popular(popularProducts: products)));
                      },
                    )

                  ],
                ),
                SizedBox(height: 30.w,),
                Row(

                  children: [
                    CardProduct(product: products[0]),
                    SizedBox(width: 15.w,),
                    CardProduct(product: products[1]),
                  ],
                ),
                SizedBox(height: 29.w,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      LocaleApp.actions.getString(context),
                      style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                              color: Color(0xFF2B2B2B),
                              fontSize: 16.sp,
                              height: 24/16.w,
                              fontWeight: FontWeight.w500

                          )
                      ),
                    ),
                    InkWell(
                      child: Text(
                        LocaleApp.all.getString(context),
                        style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              color: Color(0xFF48B2E7),
                              fontSize: 12.sp,
                              height: 16/12.w,

                            )
                        ),
                      ),
                    )

                  ],
                ),
                SizedBox(height: 20.w,),
                SizedBox(
                  height: 95.w,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                      itemCount: 6,
                      itemBuilder: (_, index){
                        return Container(
                            width: 335.w,
                            height: 95.w,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16.w)
                            ),
                            child: Image.asset('assets/Rectangle 4232.png')
                        );
                      }),
                )
              ],
            ),),
            SizedBox(height: 101.w,),


          ],
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
