import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/widgets/back_button.dart';

class Order extends StatefulWidget {
  const Order({super.key});

  @override
  State<Order> createState() => _OrderState();
}

class _OrderState extends State<Order> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtontoBack(),
                  Padding(padding: EdgeInsets.symmetric(vertical: 10.w),child: Text(
                    LocaleApp.cart.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF2B2B2B),
                            height: 24/16.w,
                            fontSize: 16.sp,

                            fontWeight: FontWeight.w500
                        )
                    ),
                  ),),
                  Container(
                    height: 44.w,
                    width: 44.w,
                  ),


                ],
              ),
              SizedBox(height: 46.w),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(16.w),
                ),
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                  ],
                ),
                
              )
            ],
          ),
        ),
      ),
    );
  }
}
