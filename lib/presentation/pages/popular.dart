import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/widgets/back_button.dart';
import 'package:session0_1/presentation/widgets/card_product.dart';

class Popular extends StatefulWidget {
  Popular({super.key, required this.popularProducts});
  List<ModelProduct> popularProducts;

  @override
  State<Popular> createState() => _PopularState();
}

class _PopularState extends State<Popular> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFF7F7F9),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 40.w,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ButtontoBack(),
                    Padding(padding: EdgeInsets.symmetric(vertical: 10.w),child: Text(
                      LocaleApp.popular.getString(context),
                      style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                          color: Color(0xFF2B2B2B),
                          height: 24/16.w,
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w500
                        )
                      ),
                    ),),
                    Container(
                      height: 44.w,
                      width: 44.w,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(40)
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 19.25.w, vertical: 16.25.w),
                        child: SvgPicture.asset('assets/heart_no.svg'),
                      ),
                    ),

                  ],
                ),
                Expanded(child:
                GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 160/184,
                    mainAxisSpacing: 15.w,
                    crossAxisSpacing: 15.w
                ),

                  itemCount: widget.popularProducts.length,
                  itemBuilder: (_, index){
                  return CardProduct(product: widget.popularProducts[index]);
                  },

                )
                )
              ]),
        )
    );
  }
}
