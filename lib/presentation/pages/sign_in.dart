import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/domain/check.dart';
import 'package:session0_1/domain/sign_in_use_case.dart';
import 'package:session0_1/presentation/dialogs.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/pages/home.dart';
import 'package:session0_1/presentation/widgets/back_button.dart';
import 'package:session0_1/presentation/widgets/text_field.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  var email = TextEditingController();
  var password = TextEditingController();
  SignInUseCase useCase = SignInUseCase();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66.w,),
              const ButtontoBack(),
              SizedBox(height: 11.w,),
              SizedBox(
                width: double.infinity,
                child: Center(
                  child:  Text(
                    LocaleApp.signinTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            color: Color(0xFF2B2B2B),
                            overflow: TextOverflow.ellipsis,
                            fontSize: 32.sp,
                            height: 38/32.w,
                            fontWeight: FontWeight.w700
                        )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 8,),
              SizedBox(
                width: double.infinity,
                child: Center(
                  child:  Text(
                    LocaleApp.signinText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          color: Color(0xFF707B81),
                          fontSize: 16.sp,
                          height: 48/32.w,
                        )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 30.w,),
              CustomField(label: LocaleApp.email.getString(context), hint: 'xyz@gmail.com', controller: email, key: Key('TextFieldEmail')),
              SizedBox(height: 30.w,),
              CustomField(label: LocaleApp.password.getString(context), hint: '••••••••', controller: password, enableobscure: true, key: Key('TextFieldPassword')),
              SizedBox(height: 12.w,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    LocaleApp.recovery.getString(context),
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          color: Color(0xFF707B81),
                          fontSize: 12.sp,
                          height: 16/12.w,
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 24.w,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50.w,
                  width: double.infinity,
                  child: FilledButton(
                    key: Key('ButtonSignIn'),
                    child: Text(
                      LocaleApp.signinButton.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Colors.white,
                              fontSize: 14.sp,
                              height: 22/14.w,
                              fontWeight: FontWeight.w600
                          )
                      ),
                    ),
                    onPressed: ()async{
                      // Check check = Check();
                      // if (!check.checkEmail(email.text)){
                      //   showError(context, 'Email incorrect');
                      //   return;
                      // }
                      // if (!check.checkPassword(password.text)){
                      //   showError(context, 'Password is empty');
                      //   return;
                      // }

                      // else{
                        showLoading(context);
                      await useCase.pressButton(
                          email.text,
                          password.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (_) => Home()));
                              },
                              (error)async{
                            hideLoading(context);
                            showError(context, error);
                              }
                      );},
                    // },

                    style: FilledButton.styleFrom(
                      backgroundColor: Color(0xFF48B2E7),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(14.w)
                      )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 209.w,),
              SizedBox(
                width: double.infinity,
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      text: LocaleApp.areYouFirst.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              color: Color(0xFF707B81),
                              fontSize: 16.sp,
                              height: 16/19.w,
                              fontWeight: FontWeight.w500
                          )
                      ),
                      children: <TextSpan>[

                        TextSpan(
                          text: LocaleApp.createAccount.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: Color(0xFF2B2B2B),
                                  fontSize: 16.sp,
                                  height: 16/19.w,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                        )
                      ],
                    ),
                  )
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
