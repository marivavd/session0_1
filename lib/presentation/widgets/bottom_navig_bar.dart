import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavig extends StatefulWidget {
  final Function(int) onSelect;
  final Function() onTapCart;
  const BottomNavig({super.key, required this.onSelect, required this.onTapCart});

  @override
  State<BottomNavig> createState() => _BottomNavigState();
}

class _BottomNavigState extends State<BottomNavig> {
  int currentIndex = 0;
  void onTap(int index){
    setState(() {
      currentIndex = index;
    });
    widget.onSelect(index);
  }
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 106.w,
      width: double.infinity,
      child: Stack(
        children: [
          Transform.translate(offset: Offset(-1.5.w, 0),
          child: ImageFiltered(
            imageFilter: ImageFilter.blur(sigmaY: 4, sigmaX: 4),
            child: SizedBox(
              width: double.infinity,
              child: SvgPicture.asset('assets/background.svg', fit: BoxFit.fill, color: Color(
                  0x1F83AAD1)),
            ),
          ),),
          Transform.translate(offset: Offset(0, 4.w),
            child: ImageFiltered(
              imageFilter: ImageFilter.blur(sigmaY: 15, sigmaX: 15),
              child: SizedBox(
                width: double.infinity,
                child: SvgPicture.asset('assets/background.svg', fit: BoxFit.fill, color: Color(
                    0x26000000)),
              ),
            ),),
          Align(
            alignment: Alignment.bottomCenter,
            child: SizedBox(
              width: double.infinity,
              height: 106.w,
              child: SvgPicture.asset('assets/background.svg', fit: BoxFit.fill),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 30.w),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: ()=>onTap(0),
                        child: SizedBox(
                          height: 24.w,
                          width: 24.w,
                          child: SvgPicture.asset('assets/home-2.svg', color: (currentIndex == 0)?
                          Color(0xFF48B2E7):Color(0xFF707B81)),
                        ),
                      ),
                      SizedBox(width: 41.w,),
                      GestureDetector(
                        onTap: ()=>onTap(1),
                        child: SizedBox(
                          height: 24.w,
                          width: 24.w,
                          child: SvgPicture.asset('assets/heart_no.svg', color: (currentIndex ==1)?
                          Color(0xFF48B2E7):Color(0xFF707B81)),
                        ),
                      )
                    ],
                  ),
                  SizedBox(width: 138.w,),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: ()=>onTap(2),
                        child: SizedBox(
                          height: 24.w,
                          width: 24.w,
                          child: SvgPicture.asset('assets/notification.svg', color: (currentIndex == 2)?
                          Color(0xFF48B2E7):Color(0xFF707B81)),
                        ),
                      ),
                      SizedBox(width: 41.w,),
                      GestureDetector(
                        onTap: ()=>onTap(3),
                        child: SizedBox(
                          height: 24.w,
                          width: 24.w,
                          child: SvgPicture.asset('assets/profile.svg', color: (currentIndex ==3)?
                          Color(0xFF48B2E7):Color(0xFF707B81)),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 50.w),
              child: GestureDetector(
                onTap: widget.onTapCart,
                child: Container(
    height: 56.w,
    width: 56.w,
    decoration: BoxDecoration(
    color: Color(0xFF48B2E7),
    borderRadius: BorderRadius.circular(30),
    boxShadow: [
    BoxShadow(
    color: Color(0x995B9EE1),
    offset: Offset(0, 8.w),
    blurRadius: 24.w
    )
    ]
    ),
    child: Padding(
    padding: EdgeInsets.all(16),
    child: SvgPicture.asset('assets/bag-2.svg'),))

              ),
            ),
          )
        ],
      ),
    );
  }
}
