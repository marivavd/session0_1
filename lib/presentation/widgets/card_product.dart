import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:session0_1/data/models/model_product.dart';
import 'package:session0_1/data/repository/shared_preferences.dart';
import 'package:session0_1/domain/cart_use_case.dart';
import 'package:session0_1/presentation/dialogs.dart';
import 'package:session0_1/presentation/pages/details.dart';

class CardProduct extends StatefulWidget {
  final ModelProduct product;
  CardProduct({super.key, required this.product});

  @override
  State<CardProduct> createState() => _CardProductState();
}

class _CardProductState extends State<CardProduct> {
  CartUseCase cartUseCase = CartUseCase();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 182.w,
      width: 160.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.w),
      ),
      child: Stack(
        children: [
          Padding(padding: EdgeInsets.only(left: 9.w, top: 9.w),
            child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
               Padding(padding: EdgeInsets.only(top: 9.w, left: 12.w, right: 21.w),
               child:  AspectRatio(
                     aspectRatio: 117.91/70,
                     child:  InkWell(
                       child: SizedBox(
                           width: double.infinity,
                           child: CachedNetworkImage(
                             fit: BoxFit.cover,
                             imageUrl: widget.product.covers.first,
                           )
                       ),
                       onTap: (){
                         Navigator.push(context, MaterialPageRoute(builder: (_) => Details(product: widget.product)));
                       },
                     )
               ),),
                SizedBox(height: 12.w,),
                Text(
                  'Best Seller',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      color: Color(0xFF48B2E7),
                      fontSize: 12.sp,
                      height: 16/12.w,
                    )
                  ),
                ),
                SizedBox(
                  height: 8.w,
                ),
                Text(
                  widget.product.title,
                  maxLines: 1,
                  style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        color: Color(0xFF6A6A6A),
                        fontSize: 16.sp,
                        height: 20/16.w,
                        fontWeight: FontWeight.w600
                      )
                  ),
                ),
                Expanded(child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                          child: Text(
                             widget.product.cost.toString(),
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    color: Color(0xFF2B2B2B),
                                    fontSize: 14.sp,
                                    height: 16/14.w,

                                )
                            ),
                          )),
                      GestureDetector(
                        onTap: ()async{
                          showLoading(context);
                          if (widget.product.isBasket){
                            widget.product.count += 1;
                            cartUseCase.updateCountInCart(widget.product.id, widget.product.count,
                                    (p0){
                              hideLoading(context);
                                    },
                                    (error)async{
                              hideLoading(context);
                              showError(context, error);
                                    });
                          }
                          else{
                            widget.product.isBasket = true;
                            cartUseCase.makeInCart(widget.product.id,
                                    (p0){
                                  hideLoading(context);
                                },
                                    (error)async{
                                  hideLoading(context);
                                  showError(context, error);
                                });
                          }
                          setState(() {

                          });
                        },
                        child: Container(
                          height: 35.5.w,
                          width: 34.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(14.w),
                              bottomRight: Radius.circular(14.w),
                            ),
                            color: Color(0xFF48B2E7)
                          ),
                          child: (!widget.product.isBasket)?Align(
    alignment: Alignment.center,
    child: Text('+',
        style: GoogleFonts.poppins(
            textStyle: TextStyle(
              color: Colors.white,
              fontSize: 32.sp,
              height: 15.32/32.w,

            )
        ),
    )
                          ):Padding(padding: EdgeInsets.all(11.w),
                          child: SvgPicture.asset('assets/cart.svg'),),
                        ),
                      )
                    ],
                  ),
                ))
              ],
            ),
          ),
          Padding(padding: EdgeInsets.only(left: 9.w, top: 9.w),child:
            GestureDetector(
              child: Container(
                  height: 28.w,
                  width: 28.w,
                  decoration: BoxDecoration(
                      color: const Color(0xFFF7F7F9),
                      borderRadius: BorderRadius.circular(40.w)
                  ),
                  child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 8.67.w, horizontal: 8.w),
                      child: (widget.product.isFavourite)?SvgPicture.asset('assets/heart.svg'):SvgPicture.asset('assets/heart_no.svg')
                  )
              ),
              onTap: ()async{
                //
                // if (widget.product.isFavourite){
                //   widget.product.isFavourite = false;
                // }
                // else{
                //   widget.product.isFavourite = true;
                // }
                // await setAllProducts();
                // setState(() {
                //
                // });
              },
            ))
        ],
      ),
    );
  }
}
