import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableobscure;
  const CustomField({super.key, required this.label, required this.hint, required this.controller, this.enableobscure = false });

  @override
  State<CustomField> createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  bool isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.raleway(
            textStyle: TextStyle(
              fontSize: 16.sp,
              height: 20/16.w,
              fontWeight: FontWeight.w500,
              color: Color(0xFF2B2B2B)
            )
          ),
        ),
        SizedBox(height: 12.w,),
        Container(
          height: 48.w,
          width: double.infinity,
          decoration: BoxDecoration(
            color: Color(0xFFF7F7F9),
            borderRadius: BorderRadius.circular(14.w)
          ),
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableobscure)?isObscure:false,
            obscuringCharacter: '•',
            decoration: InputDecoration(
              hintText: widget.hint,
              border: InputBorder.none,
              contentPadding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 14.w),
              hintStyle: GoogleFonts.poppins(
                textStyle: TextStyle(
                  fontSize: 14.sp,
                  height: 16/14.w,
                  color: Color(0xFF6A6A6A)
                ),
              ),
              suffixIconConstraints: BoxConstraints(minWidth: 34.w),
              suffixIcon: (widget.enableobscure)?GestureDetector(
                onTap: (){
                  isObscure = !isObscure;
                },
                child: SvgPicture.asset('assets/eye.svg'),
              ):null
            ),

          ),
        )
      ],
    );
  }
}
