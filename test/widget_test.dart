// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session0_1/domain/check.dart';
import 'package:session0_1/domain/utils.dart';

import 'package:session0_1/main.dart';
import 'package:session0_1/presentation/localization.dart';
import 'package:session0_1/presentation/pages/sign_in.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void main() {
  group('Test SignIn', () {
    test('bad-valid', (){
      var check = Check();
      expect(check.checkEmail('mariva.yoga@gmail.com'), 0);
      expect(check.checkPassword(''), 0);

    });
    test('good-valid', (){
      var check = Check();
      expect(check.checkEmail('marivayoga@gmail.com'), 1);
      expect(check.checkPassword('max2max2'), 1);

    });

    testWidgets('bad-validation-email-ui', (widgetTester)async{
      FlutterLocalization localization = FlutterLocalization.instance;
      localization.init(mapLocales: [
        MapLocale('ru', LocaleApp.RU),
        MapLocale('en', LocaleApp.EN)
      ],
          initLanguageCode: 'ru'
      );
      await widgetTester.runAsync(()=>widgetTester.pumpWidget(ScreenUtilInit(
        designSize: Size(375, 812),
        builder: (_, c){
          return  MaterialApp(
            supportedLocales: localization.supportedLocales,
            localizationsDelegates: localization.localizationsDelegates,
            title: 'Flutter Demo',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            home: SignIn(),
          );
        },
      )
      )
      );

      await widgetTester.pumpAndSettle();
      await widgetTester.enterText(find.byKey(const Key("TextFieldEmail")), "mariva.yoga@gmail.com");
      await widgetTester.enterText(find.byKey(const Key("TextFieldPassword")), "123456");
      await widgetTester.ensureVisible(find.byKey(Key('ButtonSignIn')));

      await widgetTester.tap(find.byKey(Key('ButtonSignIn')));
      await widgetTester.pumpAndSettle();
      expect(find.byKey(const Key("AlertDialogError")), findsOneWidget);
    });

    testWidgets('bad-validation-password-ui', (widgetTester)async{
      FlutterLocalization localization = FlutterLocalization.instance;
      localization.init(mapLocales: [
        MapLocale('ru', LocaleApp.RU),
        MapLocale('en', LocaleApp.EN)
      ],
          initLanguageCode: 'ru'
      );
      await widgetTester.pumpWidget(ScreenUtilInit(
        designSize: Size(375, 812),
        builder: (_, c){
          return  MaterialApp(
            supportedLocales: localization.supportedLocales,
            localizationsDelegates: localization.localizationsDelegates,
            title: 'Flutter Demo',
            theme: ThemeData(
              colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
              useMaterial3: true,
            ),
            home: SignIn(),
          );
        },
      )
      );
      await widgetTester.pumpAndSettle();
      await widgetTester.enterText(find.byKey(const Key("TextFieldEmail")), "marivayoga@gmail.com");
      await widgetTester.enterText(find.byKey(const Key("TextFieldPassword")), "");
      await widgetTester.ensureVisible(find.byKey(Key('ButtonSignIn')));

      await widgetTester.tap(find.byKey(Key('ButtonSignIn')));
      await widgetTester.pumpAndSettle();
      expect(find.byKey(const Key("AlertDialogError")), findsOneWidget);
    });


    // testWidgets('show error', (tester) async {
    //   await Supabase.initialize(
    //     url: 'https://ndpirkfhcctoltoveofs.supabase.co',
    //     anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im5kcGlya2ZoY2N0b2x0b3Zlb2ZzIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTM3MTYzMDcsImV4cCI6MjAyOTI5MjMwN30.AUff60YmRHhuygJXd0vUJGWpwQafI-_2JT0pGYM50jI',
    //   );
    //   await tester.runAsync(() => tester.pumpWidget(
    //       SignIn()
    //   ));
    //   tester.tap(find.byKey(Key('Button')));
    //   tester.pump();
    //   expect(find.widgetWithText(AlertDialog, 'Error'), findsOneWidget);
    // });


    // testWidgets('Тест на переход на главный экран', (widgetTester)async{
    //   await widgetTester.runAsync(() => widgetTester.pumpWidget(MyApp()
    //   ));
    //
    //   widgetTester.tap(find.byKey(Key('Button')));
    //   await widgetTester.pumpAndSettle();
    //   expect(find.widgetWithText(AlertDialog, 'Error'), findsOneWidget);
    // });

  });
}
